# NDP
NDP datacenter stack.
The doc directory contains documentation about the project.
The sim directory contains the simulator implementation of NDP which allows running experiments in various datacenter topologies. For much more information about NDP, see the [wiki](https://gitlab.com/fing-mina/datacenters/ndp/-/wikis/home).
